import mongoose from 'mongoose';
import { from } from 'rxjs';
import { contentSchema } from './schema/content.schema';

const Content = mongoose.model('Contents', contentSchema);

export const create = contentData => {
  return from(new Content(contentData).save());
};

export const find = () => {
  return from(Content.find({}));
};

export const findByIdAndUpdate = (id, update) => {
  return from(Content.findByIdAndUpdate(id, update, { new: true }));
};

export const findByIdAndDelete = id => {
  return from(Content.findByIdAndDelete(id));
};
