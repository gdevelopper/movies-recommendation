import mongoose from 'mongoose';

export const contentSchema = new mongoose.Schema({
  title: String,
  category: String,
  description: String,
  image: String,
  creationDate: Date,
  rating: Number,
  rank: Number
});
