import mongoose from 'mongoose';

export const categorySchema = new mongoose.Schema({
  title: String,
  description: String,
  creationDate: Date
});
