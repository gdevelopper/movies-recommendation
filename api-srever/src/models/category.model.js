import mongoose from 'mongoose';
import { from } from 'rxjs';
import { categorySchema } from './schema/category.schema';

const Category = mongoose.model('Categories', categorySchema);

export const create = categoryData => from(new Category(categoryData).save());

export const find = () => {
  return from(Category.find({}));
};

export const findByIdAndUpdate = (id, update) => {
  return from(Category.findByIdAndUpdate(id, update, { new: true }));
};

export const findByIdAndDelete = id => {
  return from(Category.findByIdAndDelete(id));
};
