import * as ContentModule from '../modules/content';
import * as CategoryModule from '../modules/category';
import * as OmdbApi from '../modules/omdb-api';
import * as MvdbApi from '../modules/themoviedb-api';

export const routesConfig = (app) => {
  // Content

  app.post('/content', ContentModule.create);
  app.get('/content', ContentModule.find);
  app.put('/content/update/:id', ContentModule.findByIdAndUpdate);
  app.delete('/content/delete/:id', ContentModule.findByIdAndDelete);

  // Categories

  app.post('/categories', CategoryModule.create);
  app.get('/categories', CategoryModule.find);
  app.put('/categories/update/:id', CategoryModule.findByIdAndUpdate);
  app.delete('/categories/delete/:id', CategoryModule.findByIdAndDelete);

  // OmdbAPI

  app.get('/omdbapi/:query/:limit?/:year?', OmdbApi.fetchMovies);

  // TheMovieDbAPI

  app.get('/mvdb', MvdbApi.fetchSeries);
  // Search
  app.get('/mvdb/search/:query', MvdbApi.search);
};
