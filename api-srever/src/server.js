import cors from 'cors';
import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import * as routes from './routes';

mongoose.connect('mongodb://127.0.0.1:27017/watchLsitApp', {
  useNewUrlParser: true
});

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

routes.routesConfig(app);

const port = process.env.PORT || 3001;

app.listen(port, () => console.log(`Server started on port ${port} !`));
