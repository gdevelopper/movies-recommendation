import { from } from 'rxjs';
import { map, take, toArray } from 'rxjs/operators';
import axios from 'axios';
import { Request, Response, response } from 'express';
import mvdbapi from './themoviedb.url';

export const fetchSeries: Object = (req: Request, res: Response) => {
  const query: string = req.params.query || 'nanatsu+no+taizai';
  const limit: number = parseInt(req.params.limit, 10) || 5;
  const year: number = parseInt(req.params.year) || 2019;

  let series: Content[];

  const url = `${mvdbapi.discoverTvBaseUrl}&language=en-US&sort_by=popularity.desc&first_air_date_year=${year}&page=1&vote_average.gte=7&vote_count.gte=10&with_genres=16&include_null_first_air_dates=false&with_original_language=ja`;

  axios.get(url).then((response) => {
    from(response.data.results)
      .pipe(
        map((serie: Content) => {
          const updated = { ...serie };
          updated.poster_path = `${mvdbapi.imageBaseUrl}original${serie.poster_path}`;
          delete updated.backdrop_path;
          return updated;
        }),
        take(limit),
        toArray()
      )
      .subscribe({
        next: (result) => {
          series = result;
        },
        error: (err) => {
          console.error(err);
        },
        complete: () => {
          res.status(200).json(series);
        }
      });
  });
};

export const search: Object = (req: Request, res: Response) => {
  const query = req.params.query;

  const url = `${mvdbapi.searchBaseUrl}&query=${query}`;

  let contents: Content[];

  axios.get(url).then((response) => {
    from(response.data.results)
      .pipe(
        map((content: Content) => {
          const updated = { ...content };
          updated.poster_path = `${mvdbapi.imageBaseUrl}original${content.poster_path}`;
          delete updated.backdrop_path;
          return updated;
        }),
        toArray()
      )
      .subscribe({
        next: (result) => {
          contents = result;
        },
        error: (err) => {
          console.error(err);
        },
        complete: () => {
          res.status(200).json(contents);
        }
      });
  });
};
