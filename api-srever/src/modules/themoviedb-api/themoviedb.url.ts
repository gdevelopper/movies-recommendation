export default {
  apiKey: 'a989af132a360b47e9c1d3423e3d4c8a',
  discoverMovieBaseUrl:
    'https://api.themoviedb.org/3/discover/movie?api_key=a989af132a360b47e9c1d3423e3d4c8a',
  discoverTvBaseUrl:
    'https://api.themoviedb.org/3/discover/tv?api_key=a989af132a360b47e9c1d3423e3d4c8a',
  searchBaseUrl:
    'https://api.themoviedb.org/3/search/multi?api_key=a989af132a360b47e9c1d3423e3d4c8a&page=1',
  imageBaseUrl: 'https://image.tmdb.org/t/p/'
};
