import * as ContentModel from '../models/content.model';
import moment from 'moment';

export const create = (req, res) => {
  let content;
  const body = { ...req.body, creationDate: moment(moment.utc()) };
  ContentModel.create(body).subscribe({
    next: (result) => {
      content = result;
    },
    error: (err) => {
      console.error(err);
    },
    complete: () => {
      res.status(201).json(content);
    }
  });
};

export const find = (req, res) => {
  let content;
  ContentModel.find().subscribe({
    next: (result) => {
      content = result;
    },
    error: (err) => {
      console.error(err);
    },
    complete: () => {
      res.status(200).json(content);
    }
  });
};

export const findByIdAndUpdate = (req, res) => {
  let content;
  const id = req.params.id;
  ContentModel.findByIdAndUpdate(id, req.body).subscribe({
    next: (result) => {
      content = result;
    },
    error: (err) => {
      console.error(err);
    },
    complete: () => {
      res.status(200).json(content);
    }
  });
};

export const findByIdAndDelete = (req, res) => {
  const id = req.params.id;
  ContentModel.findByIdAndDelete(id).subscribe({
    next: (result) => {},
    error: (err) => {
      console.error(err);
    },
    complete: () => {
      res.status(200).send();
    }
  });
};
