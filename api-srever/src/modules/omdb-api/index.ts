import { from } from 'rxjs';
import { take, toArray } from 'rxjs/operators';
import axios from 'axios';
import { Request, Response } from 'express';
import omdbapi from './omdbapi.url';

export const fetchMovies: Object = (req: Request, res: Response) => {
  const query: string = req.params.query;
  const limit: number = parseInt(req.params.limit, 10) || 5;
  const year: string = req.params.year || '2019';

  let movies: Object;

  console.log(`${omdbapi.baseUrl}&s=${query}&y=${year}&l=${limit}`);

  axios.get(`${omdbapi.baseUrl}&s=${query}&y=${year}`).then(response => {
    from(response.data.Search)
      .pipe(take(limit), toArray())
      .subscribe({
        next: result => {
          movies = result;
        },
        error: err => {
          console.error(err);
        },
        complete: () => {
          res.status(200).json(movies);
        }
      });
  });
};
