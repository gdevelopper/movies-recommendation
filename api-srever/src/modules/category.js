import * as CategoryModel from '../models/category.model';
import moment from 'moment';

export const create = (req, res) => {
  let category;
  const body = { ...req.body, creationDate: moment(moment.utc()) };
  CategoryModel.create(body).subscribe({
    next: result => {
      category = result;
    },
    error: err => {
      console.error(err);
    },
    complete: () => {
      res.status(201).json(category);
    }
  });
};

export const find = (req, res) => {
  let category;
  CategoryModel.find().subscribe({
    next: result => {
      category = result;
    },
    error: err => {
      console.error(err);
    },
    complete: () => {
      res.status(200).json(category);
    }
  });
};

export const findByIdAndUpdate = (req, res) => {
  let category;
  const id = req.params.id;
  CategoryModel.findByIdAndUpdate(id, req.body).subscribe({
    next: result => {
      category = result;
    },
    error: err => {
      console.error(err);
    },
    complete: () => {
      res.status(200).json(category);
    }
  });
};

export const findByIdAndDelete = (req, res) => {
  const id = req.params.id;
  CategoryModel.findByIdAndDelete(id).subscribe({
    next: result => {},
    error: err => {
      console.error(err);
    },
    complete: () => {
      res.status(200).send();
    }
  });
};
