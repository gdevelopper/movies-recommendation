import axios from 'axios';
import { createContent, fetchContent } from '../actions';

const baseURL = 'http://localhost:3001';

export const createContentURL = (dispatch, content) => {
  axios
    .post(`${baseURL}/content`, content)
    .then((result) => {
      dispatch(createContent(result.data));
    })
    .catch((error) => {
      console.log(error);
    });
};

export const fecthContentURL = (dispatch) => {
  axios
    .get(`${baseURL}/content`)
    .then((result) => {
      dispatch(fetchContent(result.data));
      console.info('DATA FETCHED');
    })
    .catch((error) => {
      console.error(error);
    });
};

export const createContent$ = (content) => {
  return axios.post(`${baseURL}/content`, content);
};

export const fetchContent$ = () => axios.get(`${baseURL}/content`);

export const fetchCategories$ = () => axios.get(`${baseURL}/categories`);

// Api's access points

export const fetchSeries$ = () => axios.get(`${baseURL}/mvdb`);

export const search$ = (query) => axios.get(`${baseURL}/mvdb/search/${query}`);
