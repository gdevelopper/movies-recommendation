export const helperFSR = ({ dispatch }) => {
  return (next) => (action) => {
    if (typeof action === 'function') {
      return action(dispatch);
    }

    return next(action);
  };
};
