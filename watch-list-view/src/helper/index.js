export const trim = (str) => {
  if (typeof str === 'string') {
    return str.trim();
  }
  return '';
};
