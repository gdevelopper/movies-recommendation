import React from 'react';
import { HomePage } from './components/home-page';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { helperFSR } from './helper/FSRmiddleware';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import Immutable from 'immutable';
import { rootReducer } from './store';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import { SearchPage } from './components/search-page';

const middlewares = [helperFSR, thunk];

const initialState = Immutable.Map();

const store = createStore(
  rootReducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middlewares))
);

function App() {
  return (
    <Provider store={store}>
      <Router>
        <>
          <nav>
            <ul
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                alignItems: 'center',
                fontWeight: 'bold',
                listStyle: 'none',
                '& > li': {
                  color: '#FFF'
                }
              }}
            >
              <li>
                <Link to="/" extact={true}>
                  Home
                </Link>
              </li>
              <li>
                <Link to="/search">Search</Link>
              </li>
            </ul>
          </nav>

          <Switch>
            <Route path="/search">
              <SearchPage />
            </Route>
            <Route path="/">
              <HomePage />
            </Route>
          </Switch>
        </>
      </Router>
    </Provider>
  );
}

export default App;
