// actions creators
import { CREATE_CONTENT, FETCH_CONTENT } from './action-types';

export const createContent = content => ({
  type: CREATE_CONTENT,
  content
});

export const fetchContent = contents => ({
  type: FETCH_CONTENT,
  contents
});
