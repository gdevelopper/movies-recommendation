/** Actions type */

export const FETCH_CONTENT = 'FETCH_CONTENT';
export const CREATE_CONTENT = 'CREATE_CONTENT';
export const UDPATE_CONTENT = 'UDPATE_CONTENT';
export const DELETE_CONTENT = 'DELETE_CONTENT';
export const FETCH_CATEGORIES = 'FETCH_CATEGORIES';

// Api's

export const FETCH_SERIES = 'FETCH_SERIES';
export const SEARCH = 'SEARCH';
