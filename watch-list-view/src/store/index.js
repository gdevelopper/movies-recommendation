import { combineReducers } from 'redux-immutable';
import { fetchContent } from '../reducers/content';
import { fetchCategories } from '../reducers/category';
import { fetchSeries, serach } from '../reducers/mvdb';

export const rootReducer = combineReducers({
  contents: fetchContent.reducer,
  categories: fetchCategories.reducer,
  series: fetchSeries.reducer,
  search: serach.reducer
});
