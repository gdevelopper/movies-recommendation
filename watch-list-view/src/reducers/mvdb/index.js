import { FETCH_SERIES, SEARCH } from '../../actions/action-types';
import { fetchSeries$, search$ } from '../../data/acces-points';
import { reduxHelper } from '../../helper/redux-request-success-failure';

export const fetchSeries = reduxHelper(FETCH_SERIES, fetchSeries$);

export const serach = reduxHelper(SEARCH, search$);
