import { FETCH_CONTENT, CREATE_CONTENT } from '../../actions/action-types';
import { reduxHelper } from '../../helper/redux-request-success-failure';
import { fetchContent$, createContent$ } from '../../data/acces-points';

// export const createContent = (state = List([]), action) => {
//   switch (action.type) {
//     case CREATE_CONTENT:
//       return List([...state.toJS(), action.content]).toJS();
//     default:
//       return state.toJS();
//   }
// };

// export const fetchContent = (state = List([]), action) => {
//   switch (action.type) {
//     case FETCH_CONTENT:
//       return List([...state, action.content]);
//     default:
//       return state;
//   }
// };

export const createContent = reduxHelper(CREATE_CONTENT, createContent$);

export const fetchContent = reduxHelper(FETCH_CONTENT, fetchContent$);
