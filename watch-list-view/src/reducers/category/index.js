import { FETCH_CATEGORIES } from '../../actions/action-types';
import { reduxHelper } from '../../helper/redux-request-success-failure';
import { fetchCategories$ } from '../../data/acces-points';

export const fetchCategories = reduxHelper(FETCH_CATEGORIES, fetchCategories$);
