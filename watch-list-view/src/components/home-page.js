import React, { useEffect } from 'react';
import { makeStyles, createStyles } from '@material-ui/core';
import { Catalog } from './catalog';
import { EditContentModal } from './editContentModal';
import { useDispatch, useSelector } from 'react-redux';
import { fetchContent } from '../reducers/content';
import { fetchCategories } from '../reducers/category';
import { fetchSeries, serach } from '../reducers/mvdb';
import { SearchFiled } from './ui/serachField';

const styleSheet = makeStyles((theme) =>
  createStyles({
    mainContainer: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      margin: 'auto',
      padding: theme.spacing(4)
    }
  })
);

export const HomePage = () => {
  const style = styleSheet();

  const dispatch = useDispatch();

  // componentDidMount hook substitute
  useEffect(() => {
    dispatch(fetchContent.action());
    console.info('CONTENT_FETCHED');
    dispatch(fetchCategories.action());
    console.info('CATEGORIES_FETCHED');
    dispatch(fetchSeries.action());
    console.info('SERIES_FETCHED');
  }, []);

  return (
    <div className={style.mainContainer}>
      <h1 style={{ color: '#FFF' }}>Most loved !</h1>
      <Catalog />
      {/* <EditContentModal /> */}
    </div>
  );
};
