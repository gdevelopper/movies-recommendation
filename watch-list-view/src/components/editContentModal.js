import React from 'react';
import { EditContentForm } from './editContentForm';
import Modal from '@material-ui/core/Modal';
import { makeStyles, createStyles } from '@material-ui/styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusSquare } from '@fortawesome/free-solid-svg-icons';
import { Button } from '@material-ui/core';

const styleSheet = makeStyles(theme =>
  createStyles({
    modal: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center'
    }
  })
);

export const EditContentModal = () => {
  const [open, setOpen] = React.useState(false);
  const classes = styleSheet();

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="contained" color="primary" onClick={handleOpen}>
        <FontAwesomeIcon size={'2x'} icon={faPlusSquare} />
      </Button>
      <Modal open={open} onClose={handleClose} className={classes.modal}>
        <EditContentForm close={handleClose} />
      </Modal>
    </div>
  );
};
