import React from 'react';
import { Modal, makeStyles, createStyles } from '@material-ui/core';
import { ContentDetails } from './contenDetails';

const styleSheet = makeStyles(() =>
  createStyles({
    modal: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center'
    }
  })
);

export const ConntentDetailsModal = ({ open, handelClose, data }) => {
  const style = styleSheet();

  return (
    <Modal open={open} onClose={handelClose} className={style.modal}>
      <ContentDetails
        image={{ src: data.poster_path, alt: data.name }}
        infos={data}
      />
    </Modal>
  );
};
