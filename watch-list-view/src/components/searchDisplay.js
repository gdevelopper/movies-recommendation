import React from 'react';
import { CardImage } from './ui/cardImage';
import { makeStyles, createStyles } from '@material-ui/core';

const styleSheet = makeStyles((theme) =>
  createStyles({
    mainContainer: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'space-around',
      margin: theme.spacing(4)
    },
    card: {
      display: 'flex',
      flexDirection: 'row',
      width: 200,
      maxWidth: 500,
      height: 180,
      borderRadius: theme.shape.borderRadius,
      margin: theme.spacing(1),
      boxShadow: theme.shadows[5],
      flexGrow: 0,
      '&:hover': {
        boxShadow: theme.shadows[10],
        width: 500,
        flexGrow: 1
      }
    },
    image: {
      height: '100%',
      width: 200
    }
  })
);

export const SerachDisplay = ({ data }) => {
  // data : series, movies, ...
  const style = styleSheet();
  const styleCard = Object.assign({ ...style.card }, style.break);
  console.log(styleCard);

  return (
    <div className={style.mainContainer}>
      {data.map((content, index) => {
        if (index % 3 === 0) {
          return (
            <>
              <div style={{ flexBasis: '100%', height: 0 }} />
              <CardImage data={content} customStyle={style} />
            </>
          );
        }
        return <CardImage data={content} customStyle={style} />;
      })}
    </div>
  );
};
