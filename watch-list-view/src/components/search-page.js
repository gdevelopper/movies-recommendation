import React from 'react';
import { SearchFiled } from './ui/serachField';
import { SerachDisplay } from './searchDisplay';
import { useSelector, useDispatch } from 'react-redux';
import { serach } from '../reducers/mvdb';
import { trim } from '../helper';

export const SearchPage = ({}) => {
  const searchData = useSelector((state) => state.getIn(['search', 'data']));

  const dispatch = useDispatch();

  return (
    <>
      <SearchFiled
        onClick={(value) => {
          dispatch(serach.action(trim(value).replace(/\s+/g, '+')));
        }}
      />
      <SerachDisplay data={searchData} />
    </>
  );
};
