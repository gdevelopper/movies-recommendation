import React from 'react';
import moment from 'moment';
import { makeStyles, createStyles, Typography, Box } from '@material-ui/core';
import { Image } from './ui/image';

const styleSheet = makeStyles((theme) =>
  createStyles({
    mainContainer: {
      display: 'flex',
      width: '70%',
      height: '35em',
      margin: 'auto',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row',
      borderRadius: theme.shape.borderRadius,
      boxShadow: theme.shadows[20],
      backgroundColor: '#FFF'
    },
    infos: { padding: theme.spacing(3), width: '70%' },
    poster: {
      height: '100%',
      borderTopLeftRadius: theme.shape.borderRadius,
      borderBottomLeftRadius: theme.shape.borderRadius
    }
  })
);

export const ContentDetails = ({ image, infos }) => {
  const style = styleSheet();

  const showedTitle = infos.name || infos.title;

  return (
    <div className={style.mainContainer}>
      <Image src={image.src} alt={showedTitle} className={style.poster} />
      <Box className={style.infos}>
        <Typography gutterBottom variant="h5" component="h2">
          {showedTitle}
        </Typography>
        {infos.original_name && (
          <Typography gutterBottom variant="subtitle2" component="h2">
            {infos.original_name}
          </Typography>
        )}
        <Typography variant="body1" component="p">
          {infos.overview}
        </Typography>
        <Typography variant="body2" component="p">
          Vote : {infos.vote_average}
        </Typography>
        <Typography variant="body2" component="p">
          First airing date : {infos.first_air_date}
        </Typography>
      </Box>
    </div>
  );
};
