import React, { useEffect } from 'react';
import { CardImage } from './ui/cardImage';
import { makeStyles, createStyles } from '@material-ui/core';
import { useSelector } from 'react-redux';

const styleSheet = makeStyles((theme) =>
  createStyles({
    cardsContainer: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-evenly',
      flexWrap: 'wrap',
      width: '90%',
      margin: 'auto',
      marginTop: theme.spacing(3),
      padding: theme.spacing(3)
    }
  })
);

export const Catalog = () => {
  const style = styleSheet();

  // const contents = useSelector(state => state.getIn(['contents', 'data']));
  const series = useSelector((state) => state.getIn(['series', 'data']));

  return (
    <div>
      {/* <iframe
        width="560"
        height="315"
        src="https://www.youtube.com/embed/Y8Wp3dafaMQ"
        frameborder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen
        title="The Dark Knight Rises: What Went Wrong? – Wisecrack Edition"
      ></iframe> */}

      <div className={style.cardsContainer}>
        {series.map((serie) => {
          return <CardImage key={serie._id} data={serie} />;
        })}
      </div>
    </div>
  );
};
