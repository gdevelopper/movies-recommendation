import React, { useState } from 'react';
import {
  Card,
  makeStyles,
  CardMedia,
  createStyles,
  CardContent,
  CardActionArea,
  Typography
} from '@material-ui/core';
import { ConntentDetailsModal } from '../contentDetailsModal';

const styleSheet = makeStyles((theme) =>
  createStyles({
    card: {
      width: 140,
      maxWidth: 250,
      height: 130,
      borderRadius: theme.spacing(1),
      boxShadow: theme.shadows[5],
      flexGrow: 0,
      '&:hover': {
        marginTop: theme.spacing(3),
        boxShadow: theme.shadows[10],
        height: 300,
        flexGrow: 0.3,
        transition: 'width 1s'
      }
    },
    image: {
      height: 130,
      width: '100%'
    }
  })
);

const getSummury = (overview) => overview && overview.substring(0, 200) + '...';

export const CardImage = ({ data, customStyle }) => {
  const style = customStyle || styleSheet();

  const [open, setOpen] = useState(false);

  const handelOpen = () => {
    setOpen(true);
  };

  const handelClose = () => {
    setOpen(false);
  };

  const { name, title, overview, poster_path } = data;

  const showedTitle = name || title;

  return (
    <>
      <Card className={style.card}>
        <CardActionArea
          onClick={() => {
            handelOpen();
            console.log('showdetails');
          }}
        >
          <CardMedia
            image={poster_path}
            title={showedTitle}
            className={style.image}
          />
        </CardActionArea>
        <CardContent>
          <Typography gutterBottom variant="h6" component="h2">
            {showedTitle}
          </Typography>
          <Typography variant="body2" component="p">
            {getSummury(overview)}
          </Typography>
        </CardContent>
      </Card>
      {open && (
        <ConntentDetailsModal
          open={open}
          handelClose={handelClose}
          data={data}
        />
      )}
    </>
  );
};
