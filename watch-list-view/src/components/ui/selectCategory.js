import React from 'react';
import { Autocomplete } from '@material-ui/lab';
import { TextField } from '@material-ui/core';
import { spacing } from './spacing';

export const SelectCategory = props => {
  const options = props.data.map(option => {
    const firstLetter = option.title[0].toUpperCase();
    return {
      firstLetter: /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
      ...option
    };
  });

  return (
    <Autocomplete
      id="grouped-demo"
      options={options.sort(
        (a, b) => -b.firstLetter.localeCompare(a.firstLetter)
      )}
      groupBy={option => option.firstLetter}
      getOptionLabel={option => {
        props.setCategory(option.title);
        return option.title;
      }}
      renderInput={params => (
        <TextField
          {...params}
          label="With categories"
          variant="outlined"
          fullWidth
          className={props.className}
          required={props.required}
        />
      )}
    />
  );
};
