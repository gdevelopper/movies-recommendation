import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { Rating } from '@material-ui/lab';

export const RatingTitled = props => {
  return (
    <Box
      component="fieldset"
      borderColor="transparent"
      className={props.className}
    >
      <Typography component="legend">{props.title}</Typography>
      <Rating
        name="simple-controlled"
        value={props.value}
        onChange={(event, newValue) => {
          props.onChange(newValue);
        }}
      />
    </Box>
  );
};
