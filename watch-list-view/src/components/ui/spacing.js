export const spacing = {
  s: 5,
  m: 10,
  l: 15,
  xl: 20,
  xxl: 25
};
