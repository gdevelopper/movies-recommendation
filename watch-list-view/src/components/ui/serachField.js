import React, { useState } from 'react';
import { makeStyles, createStyles, ButtonBase } from '@material-ui/core';

const styleSheet = makeStyles((theme) =>
  createStyles({
    serachFiled: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      width: '40%',
      margin: 'auto',
      borderRadius: 20,
      padding: 0,
      '&:focus-within': {
        width: '50%',
        transition: 'width 1s'
      }
    },
    input: {
      width: '90%',
      height: 30,
      padding: theme.spacing(1),
      paddingLeft: theme.spacing(2),
      border: 'none',
      borderTopLeftRadius: 20,
      borderBottomLeftRadius: 20
    },
    btnSearch: {
      fontSize: '2em',
      width: '2.5em',
      padding: theme.spacing(1),
      border: 'none',
      backgroundImage: 'linear-gradient(90deg,#ff8a00,#e52e71)',
      borderTopRightRadius: 20,
      borderBottomRightRadius: 20,
      cursor: 'pointer',
      '&:hover': {
        backgroundImage: 'linear-gradient(180deg,#ffaa00,#e22e71)'
      }
    },
    span: {
      '&:focus': {
        outline: 'none',
        border: 'none'
      }
    }
  })
);

export const SearchFiled = ({ onClick }) => {
  const style = styleSheet();
  const [value, setValue] = useState();
  return (
    <div className={style.serachFiled}>
      <input
        className={style.input}
        type="text"
        placeholder="Serach ..."
        onInput={(e) => {
          setValue(e.target.value);
        }}
      />
      <ButtonBase
        className={style.btnSearch}
        onClick={(e) => {
          e.preventDefault();
          onClick(value);
        }}
      >
        <span className={style.span}>🔍</span>
      </ButtonBase>
    </div>
  );
};
