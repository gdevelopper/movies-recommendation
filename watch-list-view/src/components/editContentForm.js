import React, { useState } from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { TextField, Button } from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import { useDispatch, useSelector } from 'react-redux';
import { trim } from '../helper';
import { fetchContent, createContent } from '../reducers/content';
import { SelectCategory } from './ui/selectCategory';
import { RatingTitled } from './ui/rating';

const styleSheet = makeStyles(theme =>
  createStyles({
    container: {
      display: 'flex',
      width: '350px',
      height: '520px',
      padding: theme.spacing(3),
      justifyContent: 'center',
      flexDirection: 'column',
      backgroundColor: '#FFF',
      borderRadius: theme.spacing(2)
    },
    items: {
      marginBottom: theme.spacing(2)
    }
  })
);

export const EditContentForm = ({ close }) => {
  const style = styleSheet();
  const [rating, setValue] = useState(2);
  const [title, setTitle] = useState('Title');
  const [description, setDescription] = useState('Description');
  const [category, setCategory] = useState('Anime');
  const [image, setImage] = useState('cat');

  const dispatch = useDispatch();

  const categories = useSelector(state => state.getIn(['categories', 'data']));

  const saveContent = () => {
    const content = {
      title,
      description,
      rating,
      category,
      image
    };

    dispatch(createContent.action(content)).then(result => {
      // TODO: refacto to update the immutable state
      dispatch(fetchContent.action());
    });

    close();
  };

  const canSave = !!title & !!description & !!category & !!image;

  return (
    <div className={style.container}>
      <form noValidate autoComplete="off">
        <TextField
          id="standard-basic"
          label="Title"
          fullWidth
          variant="outlined"
          className={style.items}
          onChange={e => {
            setTitle(trim(e.target.value));
          }}
          required
        />
        <SelectCategory
          setCategory={setCategory}
          data={categories}
          required={true}
          className={style.items}
        />
        <TextField
          multiline
          fullWidth
          label="Description"
          rows={10}
          variant="outlined"
          className={style.items}
          onChange={e => {
            setDescription(trim(e.target.value));
          }}
          required
        />
        <TextField
          id="standard-basic"
          label="Image"
          fullWidth
          variant="outlined"
          className={style.items}
          onChange={e => {
            setImage(trim(e.target.value));
          }}
          required
        />
        <RatingTitled
          className={style.items}
          title={'Rating'}
          value={rating}
          onChange={setValue}
        />
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <Button
            variant="contained"
            color="primary"
            startIcon={<SaveIcon />}
            onClick={saveContent}
            disabled={!canSave}
          >
            Save
          </Button>
        </div>
      </form>
    </div>
  );
};
